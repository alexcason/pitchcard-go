Pitchcard
=========

Pitch your idea. Get Feedback.

Todo
----

- [ ] User profile (lists pitches, allows deletions, account deactivation)
- [ ] Like/dislike poll feedback on pitch
- [ ] Public/private option on pitch creation
- [ ] Showcase/trending page for public pitches
- [ ] Encrypt pitch details

- [ ] Tidy up project file structure
- [ ] Refactor authentication to eliminate checking in route functions
- [ ] Refactor pitch/comment author retrieval (awful)