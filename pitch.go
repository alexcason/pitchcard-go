package main

import (
  "labix.org/v2/mgo/bson"
  "time"
)

type Pitch struct {
  Guid string
  Users_ID bson.ObjectId
  Title string `form:"title"`
  Problem string `form:"problem"`
  Reason string `form:"reason"`
  Solution string `form:"solution"`
  Difference string `form:"difference"`
  Private bool `form:"private"`
  Comments []Comment `bson:"comments"`
  Likes []Vote
  Dislikes []Vote
  Created_At time.Time
}

type PitchViewModel struct {
  Guid string
  User User
  Title string
  Problem string
  Reason string
  Solution string
  Difference string
  Private bool
  Comments []CommentViewModel
  Likes_Count int
  Dislikes_Count int
  Created_At string
}

type Comment struct {
  ID bson.ObjectId `bson:"_id,omitempty"`
  Users_ID bson.ObjectId
  Body string `form:"body"`
  Comments_ID string `form:"comments_id"`
  Comments []Comment
  Created_At time.Time
}

type CommentViewModel struct {
  ID string
  User User
  Body string
  Comments []CommentViewModel
  Created_At string
}