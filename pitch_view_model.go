package main

type GetPitchViewModel struct {
  Pitch PitchViewModel
  LoggedIn bool
  ShareUrl string
}