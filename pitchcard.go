package main

import (
  "github.com/codegangsta/martini"
  "github.com/codegangsta/martini-contrib/render"
  "github.com/codegangsta/martini-contrib/binding"
  "labix.org/v2/mgo"
  "labix.org/v2/mgo/bson"
  "net/http"
)

func main() {
  m := martini.Classic()
  m.Use(render.Renderer(render.Options {
    // define the layout template for use by all pages
    Layout: "layout",
  }))
  m.Use(DB())
  m.Use(func(w http.ResponseWriter, r *http.Request, c martini.Context, db *mgo.Database) {
    // get user (if available) and inject into request
    user := User{}
    if c, _ := r.Cookie("auth"); c != nil {
      db.C("users").Find(bson.M{"credentials.token": c.Value}).One(&user)
    }
    c.Map(user)
  })

  initOAuthClient()

  m.Get("/", getIndex)
  m.Get("/signin", getSignin)
  m.Get("/callback", getOAuthCallback)
  m.Get("/signout", getSignout)
  m.Get("/account", getAccount)
  m.Post("/pitches", binding.Form(Pitch{}), createPitch)
  m.Post("/pitches/:id/comments", binding.Form(Comment{}), createComment)
  m.Post("/pitches/:id/likes", likePitch);
  m.Post("/pitches/:id/dislikes", dislikePitch);
  m.Get("/:id", getPitch)

  m.Run()
}