$(document).ready(function() {
  formatAllTimestamps();
  initVotingControls();

  // recalculate all times every minute
  setInterval(function() {
    formatAllTimestamps();
  }, 60000);

  $('.comment-form textarea').focusin(function() {
    $commentTextarea = $(this);
    var count = $commentTextarea.val().length;

    if (count == 0) {
      $commentTextarea.animate({height:'120px'}, 200);
    }
  });

  $('.comment-form textarea').focusout(function() {
    $commentTextarea = $(this);
    var count = $commentTextarea.val().length;

    if (count == 0) {
      $commentTextarea.animate({height:'45px'}, 200);
    }
  });

  $('.comments').on('focusout', '.comment-reply-form textarea', function() {
    $commentTextarea = $(this);
    var count = $commentTextarea.val().length;

    if (count == 0) {
      $commentTextarea.closest('.comment-reply-form').slideUp(200);
    }
  });

  $('.comments').on('click', '.comment-reply-control', function () {
    var $commentReplyControls = $(this).parent().siblings('.comment-reply-form');

    commentReplyClick($commentReplyControls);

    return false;
  });

  $('.comments').on('click', '.comment-reply-reply-control', function() {
    var $commentReplyControls = $(this).closest('.comment-replies').siblings('.comment-reply-form');

    commentReplyClick($commentReplyControls);

    return false;
  });

  $('.vote-form').submit(function() {
    $frm = $(this);

    $.ajax({
      type: $frm.attr('method'),
      url: $frm.attr('action'),
      data: $frm.serialize(),
      success: function (data) {
        var pitchGuid = $('.pitch').data('guid');

        $.cookie('v_' + pitchGuid, 1);

        $('.vote-form').hide();
        
        $('.like-count').text(data.likeCount);
        $('.dislike-count').text(data.dislikeCount);
        $('.vote-result').show();
      }
    });

    return false;
    e.preventDefault();
  });

  $('.comment-form').submit(function() {
    $frm = $(this);

    $.ajax({
      type: $frm.attr('method'),
      url: $frm.attr('action'),
      data: $frm.serialize(),
      success: function (data) {
        var commentHtml = '<li class="comment-item">' +
                            '<header>' +
                              '<h1>' + data.userName + '</h1>' +
                            '</header>' +
                            '<p>' + data.commentBody + '</p>' +
                            '<section class="comment-details">' +
                              'about <time datetime="' + data.commentCreatedAt + '">' + moment.utc(data.commentCreatedAt).fromNow() + '</time>' +
                              '<a href="#" class="comment-reply-control">Reply</a>' +
                            '</section>' +
                            '<ul class="comment-replies">' +
                            '</ul>' +
                            '<form action="/pitches/' + data.pitchGuid + '/comments" method="POST" class="pure-form pure-form-stacked comment-reply-form" style="display: none;">' +
                              '<fieldset>' +
                                '<input type="hidden" name="comments_id" value="' + data.commentId + '" />' +
                                '<textarea name="body" placeholder="Write a comment..." class="pure-input-1 expanding" autocomplete="off"></textarea>' +
                                '<input type="submit" value="Submit" class="pure-button pure-button-primary">' +
                              '</fieldset>' +
                            '</form>' +
                          '</li>';

        // reset comment interface
        var $textArea = $frm.find('textarea');
        $textArea.val('');
        $textArea.blur();

        setTimeout(function() {
          $comments = $('.comments').append(commentHtml);

          // scroll page to show new comment
          var scrollVal = $comments.offset().top - viewport().height + $comments.height() + 25;
          var scrollPos = $(window).scrollTop();

          if (scrollVal > scrollPos) {
            $('html, body').animate({scrollTop: scrollVal}, 200);
          }

        }, 300);
      }
    });

    return false;
    e.preventDefault();
  });

  $('.comments').on('submit', '.comment-reply-form', function() {
    $frm = $(this);

    $.ajax({
      type: $frm.attr('method'),
      url: $frm.attr('action'),
      data: $frm.serialize(),
      success: function (data) {
        var commentHtml = '<li class="comment-reply-item">' +
                            '<header>' +
                              '<h1>' + data.userName + '</h1>' +
                            '</header>' +
                            '<p>' + data.commentBody + '</p>' +
                            '<section class="comment-details">' +
                              'about <time datetime="' + data.commentCreatedAt + '">' + moment.utc(data.commentCreatedAt).fromNow() + '</time>' +
                              '<a href="#" class="comment-reply-reply-control">Reply</a>' +
                            '</section>' +
                          '</li>';

        // reset comment interface
        var $textArea = $frm.find('textarea');
        $textArea.val('');
        $textArea.blur();

        setTimeout(function() {
          $commentReplies = $frm.siblings('.comment-replies').append(commentHtml);

          // scroll page to show new comment
          var scrollVal = $commentReplies.offset().top - viewport().height + $commentReplies.height() + 25;
          var scrollPos = $(window).scrollTop();

          if (scrollVal > scrollPos) {
            $('html, body').animate({scrollTop: scrollVal}, 200);
          }

        }, 300);
      }
    });

    return false;
    e.preventDefault();
  });

  function commentReplyClick($commentReplyControls) {
    $commentReplyControls.show(0, function() {
      if ($commentReplyControls.is(':visible')) {
        scrollVal = $commentReplyControls.offset().top - viewport().height + $commentReplyControls.height() + 25;
        scrollPos = $(window).scrollTop();

        if (scrollVal > scrollPos) {
          $('html, body').animate({scrollTop: scrollVal}, 200);
        }

        $commentReplyControls.find('textarea').focus();
      }
    });
  }

  function formatAllTimestamps() {
    $('time').each(function() {
      $el = $(this);

      var datetime = $el.attr('datetime');
      var timeago = moment.utc(datetime).fromNow();

      $el.text(timeago);
    });
  }

  function initVotingControls() {
    var pitchGuid = $('.pitch').data('guid');
    var cookie = $.cookie('v_' + pitchGuid);

    if (cookie != undefined) {
      $('.vote-form').hide();
      $('.vote-result').show();
    }
  }
  
  function viewport() {
    var e = window, a = 'inner';

    if ( !( 'innerWidth' in window ) ) {
      a = 'client';
      e = document.documentElement || document.body;
    }
    
    return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
  }

});