package main

import (
  "github.com/codegangsta/martini"
  "github.com/codegangsta/martini-contrib/render"
  "github.com/nu7hatch/gouuid"
  "labix.org/v2/mgo"
  "labix.org/v2/mgo/bson"
  "net/http"
  "strconv"
  "strings"
  "time"
)

func getIndex(r render.Render, w http.ResponseWriter, db *mgo.Database, user User) {
  pitch := Pitch{}
  db.C("pitches").Find(bson.M{"users_id": user.ID}).Sort("-_id").One(&pitch)

  indexViewModel := IndexViewModel {
    User: user,
    LatestPitch: pitch,
  }

  r.HTML(http.StatusOK, "index", indexViewModel)
}

type IndexViewModel struct {
  User User
  LatestPitch Pitch
}

func getPitch(params martini.Params, r render.Render, req *http.Request, db *mgo.Database, user User) {
  pitch := Pitch{}
  // find pitch by id param and take first result
  err := db.C("pitches").Find(bson.M{"guid": params["id"]}).One(&pitch)
  if err != nil {
    panic(err)
  }

  // construct comment view models
  // TODO: rewrite functionality to collate all required users from pitches, comments and replies and retrieve required users in single database query
  pitchComments := []CommentViewModel{}
  for _, comment := range pitch.Comments {

    commentReplies := []CommentViewModel{}
    for _, commentReply := range comment.Comments {
      commentReplyUser := User{}
      err := db.C("users").Find(bson.M{"_id": commentReply.Users_ID}).One(&commentReplyUser)
      if err != nil {
        panic(err)
      }

      commentReplyViewModel := CommentViewModel{
        ID: bson.ObjectId.Hex(commentReply.ID),
        User: commentReplyUser,
        Body: commentReply.Body,
        Created_At: comment.Created_At.Format(time.RFC3339),
      }

      commentReplies = append(commentReplies, commentReplyViewModel)
    }

    commentUser := User{}
    err := db.C("users").Find(bson.M{"_id": comment.Users_ID}).One(&commentUser)
    if err != nil {
      panic(err)
    }

    commentViewModel := CommentViewModel{
      ID: bson.ObjectId.Hex(comment.ID),
      User: commentUser,
      Body: comment.Body,
      Comments: commentReplies,
      Created_At: comment.Created_At.Format(time.RFC3339),
    }
    pitchComments = append(pitchComments, commentViewModel)
  }

  // construct pitch view model
  pitchUser := User{}
  err = db.C("users").Find(bson.M{"_id": pitch.Users_ID}).One(&pitchUser)
  if err != nil {
    panic(err)
  }

  // TODO: refactor into common function
  pitchLikeCount := len(pitch.Likes)
  pitchDislikeCount := len(pitch.Dislikes)
  pitchTotalCount := float32(pitchLikeCount) + float32(pitchDislikeCount)
  pitchLikePercentage := int((float32(pitchLikeCount) / pitchTotalCount) * 100)
  pitchDislikePercentage := int((float32(pitchDislikeCount) / pitchTotalCount) * 100)

  pitchViewModel := PitchViewModel{
    Guid: pitch.Guid,
    User: pitchUser,
    Title: pitch.Title,
    Problem: pitch.Problem,
    Reason: pitch.Reason,
    Solution: pitch.Solution,
    Difference: pitch.Difference,
    Private: pitch.Private,
    Comments: pitchComments,
    Likes_Count: pitchLikePercentage,
    Dislikes_Count: pitchDislikePercentage,
    Created_At: pitch.Created_At.Format(time.RFC3339),
  }

  // construct get pitch view model
  loggedIn := false
  if user.TwitterID != "" {
    loggedIn = true
  }
  shareUrl := "http://" + req.Host + "/" + pitch.Guid
  getPitchViewModel := GetPitchViewModel{
    Pitch: pitchViewModel,
    LoggedIn: loggedIn,
    ShareUrl: shareUrl,
  }

  r.HTML(http.StatusOK, "pitch", getPitchViewModel)
}

func createPitch(r render.Render, pitch Pitch, db *mgo.Database, user User, w http.ResponseWriter, req *http.Request) {
  if user.TwitterID != "" {
    if pitch.Title != "" && pitch.Problem != "" && pitch.Solution != "" && pitch.Difference != "" {
      // generate a uuid for the new pitch
      u, err := uuid.NewV4()
      if err != nil {
        panic(err)
      }
      pitch.Guid = strings.Replace(u.String(), "-", "", -1)
      pitch.Users_ID = user.ID
      pitch.Created_At = time.Now()
      if req.FormValue("private") == "on" {
        pitch.Private = true
      }

      err = db.C("pitches").Insert(pitch)

      if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
      } else {
        r.Redirect("/" + pitch.Guid, http.StatusFound)
      }
    } else {
      r.Redirect("/", http.StatusBadRequest)
    }
  } else {
    r.Redirect("/", http.StatusUnauthorized)
  }
}

func createComment(r render.Render, params martini.Params, comment Comment, w http.ResponseWriter, req *http.Request, db *mgo.Database, user User) {
  pitchGuid := params["id"]
  // redirectPath := "/" + pitchGuid
  commentsId := comment.Comments_ID

  if user.TwitterID != "" {
    if comment.Body != "" {
      comment.ID = bson.NewObjectId()
      comment.Users_ID = user.ID
      comment.Comments_ID = ""
      comment.Created_At = time.Now()

      if commentsId == "" {
        // no parent comment id - create new top level comment
        err := db.C("pitches").Update(bson.M{"guid": pitchGuid}, bson.M{"$push": bson.M{"comments": comment}})
        if err!= nil {
          panic(err)
        }
        
      } else {
        // parent comment id included - create new comment reply
        if bson.IsObjectIdHex(commentsId) {
          err := db.C("pitches").Update(bson.M{"guid": pitchGuid, "comments._id": bson.ObjectIdHex(commentsId)}, bson.M{"$push": bson.M{"comments.$.comments": comment}})
          if err != nil {
            panic(err)
          }
        } else {
          r.JSON(http.StatusBadRequest, nil)
        }      
      }

      r.JSON(http.StatusOK, map[string]interface{}{"userName": user.Name, "pitchGuid": pitchGuid, "commentId": comment.ID, "commentBody": comment.Body, "commentCreatedAt": comment.Created_At.Format(time.RFC3339)})
    } else {
      r.JSON(http.StatusBadRequest, nil)
    }
  } else {
    r.JSON(http.StatusUnauthorized, nil)
  }
}

func likePitch(r render.Render, params martini.Params, db *mgo.Database) {
  pitchGuid := params["id"]
  pitch := Pitch{}

  vote := Vote{
    Created_At: time.Now(),
  }

  err := db.C("pitches").Update(bson.M{"guid": pitchGuid}, bson.M{"$push": bson.M{"likes": vote}})
  if err != nil {
    panic(err)
  }

  err = db.C("pitches").Find(bson.M{"guid": pitchGuid}).One(&pitch)
  if err != nil {
    panic(err)
  }

  // TODO: refactor into common function
  pitchLikeCount := len(pitch.Likes)
  pitchDislikeCount := len(pitch.Dislikes)
  pitchTotalCount := float32(pitchLikeCount) + float32(pitchDislikeCount)
  pitchLikePercentage := int((float32(pitchLikeCount) / pitchTotalCount) * 100)
  pitchDislikePercentage := int((float32(pitchDislikeCount) / pitchTotalCount) * 100)

  r.JSON(http.StatusOK, map[string]interface{}{"likeCount": pitchLikePercentage, "dislikeCount": pitchDislikePercentage});
}

func dislikePitch(r render.Render, params martini.Params, db *mgo.Database) {
  pitchGuid := params["id"]
  pitch := Pitch{}

  vote := Vote{
    Created_At: time.Now(),
  }

  err := db.C("pitches").Update(bson.M{"guid": pitchGuid}, bson.M{"$push": bson.M{"dislikes": vote}})
  if err!= nil {
    panic(err)
  }

  err = db.C("pitches").Find(bson.M{"guid": pitchGuid}).One(&pitch)
  if err != nil {
    panic(err)
  }

  // TODO: refactor into common function
  pitchLikeCount := len(pitch.Likes)
  pitchDislikeCount := len(pitch.Dislikes)
  pitchTotalCount := float32(pitchLikeCount) + float32(pitchDislikeCount)
  pitchLikePercentage := int((float32(pitchLikeCount) / pitchTotalCount) * 100)
  pitchDislikePercentage := int((float32(pitchDislikeCount) / pitchTotalCount) * 100)

  r.JSON(http.StatusOK, map[string]interface{}{"likeCount": pitchLikePercentage, "dislikeCount": pitchDislikePercentage});
}

func getAccount(r render.Render, db *mgo.Database, user User) {
  if user.TwitterID != "" {
    pitches := []Pitch{}
    db.C("pitches").Find(bson.M{"users_id": user.ID}).All(&pitches)

    getAccountViewModel := GetAccountViewModel{
      User: user,
      Pitches: pitches,
    }

    r.HTML(http.StatusOK, "account", getAccountViewModel)
  } else {
    r.Redirect("/", http.StatusUnauthorized)
  }
}

type GetAccountViewModel struct {
  User User
  Pitches []Pitch
}

// gets the OAuth temp credentials and redirects the user to Twitter's authentication page.
func getSignin(r render.Render, w http.ResponseWriter, req *http.Request, db *mgo.Database) {
  referer := req.Referer()
  if referer == "" {
    referer = "/"
  }
  callback := "http://" + req.Host + "/callback?referer=" + referer
  tempCred, err := signinOAuthClient.RequestTemporaryCredentials(http.DefaultClient, callback, nil)
  if err != nil {
    http.Error(w, "Error getting temp cred, " + err.Error(), http.StatusInternalServerError)
    return
  }
  putCredentials(tempCred, db)

  r.Redirect(signinOAuthClient.AuthorizationURL(tempCred, nil), http.StatusFound)
}

// handles callbacks from the OAuth server.
func getOAuthCallback(r render.Render, db *mgo.Database, w http.ResponseWriter, req *http.Request) {
  tempCred := getCredentials(req.FormValue("oauth_token"), db)
  if tempCred == nil {
    http.Error(w, "Unknown oauth_token.", http.StatusInternalServerError)
    return
  }
  deleteCredentials(tempCred.Token, db)
  tokenCred, _, err := oauthClient.RequestToken(http.DefaultClient, tempCred, req.FormValue("oauth_verifier"))
  if err != nil {
    http.Error(w, "Error getting request token, " + err.Error(), http.StatusInternalServerError)
    return
  }

  // putCredentials(tokenCredentials, db)

  http.SetCookie(w, &http.Cookie{
    Name:     "auth",
    Path:     "/",
    HttpOnly: true,
    Value:    tokenCred.Token,
    Expires:  time.Now().Add(730*24*time.Hour),
  })
  if tokenCred != nil {
    twitterUser := TwitterUser{}
    err := apiGet(tokenCred, "https://api.twitter.com/1.1/account/verify_credentials.json", nil, &twitterUser)
    if err != nil {
      http.Error(w, "Error getting user details, " + err.Error(), http.StatusInternalServerError)
      return
    }
    // insert or update user in database
    user := User{
      TwitterID: strconv.FormatInt(*twitterUser.Id, 10),
      Name: *twitterUser.Name,
      Description: *twitterUser.Description,
      Location: *twitterUser.Location,
      ProfileUrl: "https://twitter.com/" + *twitterUser.Screen_name,
      ProfileImageUrl: *twitterUser.Profile_image_url_https,
      Credentials: tokenCred,
    }
    info, err := db.C("users").Upsert(bson.M{"twitterid": user.TwitterID}, user)
    if err != nil {
      panic(err)
    }
    if info.Updated == 0 {
      // TODO: update with token/secret
    }
  }

  referer := req.FormValue("referer")
  redirectPath := "/"
  if referer != "" {
    redirectPath = referer
  }

  r.Redirect(redirectPath, http.StatusFound)
}

func getSignout(r render.Render, w http.ResponseWriter, req *http.Request, db *mgo.Database, user User) {
  db.C("users").UpdateId(user.ID, bson.M{"$set": bson.M{"credentials": nil}})

  // delete cookies
  http.SetCookie(w, &http.Cookie{
    Name:     "auth",
    Path:     "/",
    HttpOnly: true,
    MaxAge:   -1,
  })

  // redirect back to referer page
  referer := req.Referer()
  if referer == "" {
    referer = "/"
  }
  r.Redirect(referer, http.StatusFound)
}