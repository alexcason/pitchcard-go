package main

import (
  "encoding/json"
  "flag"
  "fmt"
  "github.com/garyburd/go-oauth/oauth"
  "io/ioutil"
  "labix.org/v2/mgo"
  "labix.org/v2/mgo/bson"
  "log"
  "net/http"
  "net/url"
)

var oauthClient = oauth.Client{
  TemporaryCredentialRequestURI: "https://api.twitter.com/oauth/request_token",
  ResourceOwnerAuthorizationURI: "https://api.twitter.com/oauth/authorize",
  TokenRequestURI:               "https://api.twitter.com/oauth/access_token",
}

var signinOAuthClient oauth.Client

var credPath = flag.String("config", "config.json", "Path to configuration file containing the application's credentials.")

func readCredentials() error {
  b, err := ioutil.ReadFile(*credPath)
  if err != nil {
    return err
  }
  return json.Unmarshal(b, &oauthClient.Credentials)
}

func putCredentials(cred *oauth.Credentials, db *mgo.Database) {
  err := db.C("credentials").Insert(cred)
  if err != nil {
    panic(err)
  }
}

func getCredentials(token string, db *mgo.Database) *oauth.Credentials {
  var cred *oauth.Credentials
  err := db.C("credentials").Find(bson.M{"token": token}).One(&cred)
  if err != nil {
    panic(err)
  }

  return cred
}

func deleteCredentials(token string, db *mgo.Database) {
  err := db.C("credentials").Remove(bson.M{"token": token})
  if err != nil {
    panic(err)
  }
}

// apiGet issues a GET request to the Twitter API and decodes the response JSON to data.
func apiGet(cred *oauth.Credentials, urlStr string, form url.Values, data interface{}) error {
  resp, err := oauthClient.Get(http.DefaultClient, cred, urlStr, form)
  if err != nil {
    return err
  }
  defer resp.Body.Close()
  return decodeResponse(resp, data)
}

// decodeResponse decodes the JSON response from the Twitter API.
func decodeResponse(resp *http.Response, data interface{}) error {
  if resp.StatusCode != 200 {
    p, _ := ioutil.ReadAll(resp.Body)
    return fmt.Errorf("get %s returned status %d, %s", resp.Request.URL, resp.StatusCode, p)
  }
  return json.NewDecoder(resp.Body).Decode(data)
}

// // authHandler reads the auth cookie and invokes a handler with the result.
// type authHandler struct {
//   handler  func(w http.ResponseWriter, req *http.Request, c *oauth.Credentials, ren render.Render, db *mgo.Database)
//   optional bool
// }

// func (h *authHandler) ServeHTTP(w http.ResponseWriter, r *http.Request, ren render.Render, db *mgo.Database) {
//   var cred *oauth.Credentials
//   if c, _ := r.Cookie("auth"); c != nil {
//     cred = getCredentials(c.Value)
//   }
//   if cred == nil && !h.optional {
//     http.Error(w, "Not logged in.", 403)
//     return
//   }
//   h.handler(w, r, cred, ren, db)
// }

func initOAuthClient() {
  if err := readCredentials(); err != nil {
    log.Fatalf("Error reading configuration, %v", err)
  }
  // use a different auth URL for "Sign in with Twitter."
  signinOAuthClient = oauthClient
  signinOAuthClient.ResourceOwnerAuthorizationURI = "https://api.twitter.com/oauth/authenticate"
}