package main

import (
  "github.com/garyburd/go-oauth/oauth"
  "labix.org/v2/mgo/bson"
)

type User struct {
  ID bson.ObjectId `bson:"_id,omitempty"`
  TwitterID string
  Name string
  Description string
  Location string
  ProfileUrl string
  ProfileImageUrl string
  Credentials *oauth.Credentials
}